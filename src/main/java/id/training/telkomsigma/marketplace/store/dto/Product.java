/**
 * 
 */
package id.training.telkomsigma.marketplace.store.dto;

import java.math.BigDecimal;

import lombok.Data;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Data
public class Product {
	private String id;
    private String code;
    private String name;
    private BigDecimal weight;
    private BigDecimal price;
}
