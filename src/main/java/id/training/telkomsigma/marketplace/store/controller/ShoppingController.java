/**
 * 
 */
package id.training.telkomsigma.marketplace.store.controller;

import java.math.BigDecimal;
import java.util.LinkedHashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.SessionAttributes;

import id.training.telkomsigma.marketplace.store.backendservices.CatalogService;
import id.training.telkomsigma.marketplace.store.dto.CartItem;
import id.training.telkomsigma.marketplace.store.dto.Product;
import id.training.telkomsigma.marketplace.store.dto.PurchaseOrder;
import id.training.telkomsigma.marketplace.store.dto.Shipment;
import id.training.telkomsigma.marketplace.store.dto.ShoppingCart;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id:
 */
@RestController
@SessionAttributes("cart")
public class ShoppingController {

	private static final Logger LOG = LoggerFactory.getLogger(ShoppingController.class);
	
	@Autowired
	private CatalogService catalogService;

	@ModelAttribute("cart")
	public ShoppingCart createCart() {
		return new ShoppingCart();
	}

	@GetMapping("/api/product/{id}")
	public Map<String, Object> productDetail(@PathVariable("id") String product) {
		@SuppressWarnings({ "unchecked", "rawtypes" })
		Map<String, Object> data = new LinkedHashMap();

		Product p = catalogService.cariProdukById(product);
		data.put("product", p);
		data.put("photos", catalogService.fotoProdukById(product));

		LOG.info("Berhasil mengambil data product {}", p);
		
		return data;
	}

	@PostMapping("/api/add")
	public ShoppingCart addToCart(@RequestParam("product") String product, @RequestParam("jumlah") Integer jumlah,
			@ModelAttribute("cart") ShoppingCart cart) {
		Product p001 = new Product();
		p001.setId(product);
		p001.setCode("P-001");
		p001.setName("Product 001");
		p001.setWeight(new BigDecimal(23.4));
		p001.setPrice(new BigDecimal(12345.67));

		CartItem ci = new CartItem();
		ci.setProduct(p001);
		ci.setJumlah(jumlah);

		cart.getIsiCart().add(ci);

		return cart;
	}

	@GetMapping("/api/cart")
	public ShoppingCart viewCart(@ModelAttribute("cart") ShoppingCart cart) {
		return cart;
	}

	@PostMapping("/api/order")
	public PurchaseOrder order(@RequestParam String shipping, @RequestParam String jenis,
			@ModelAttribute("cart") ShoppingCart cart) {
		PurchaseOrder po = new PurchaseOrder();
		po.setDaftarBelanja(cart.getIsiCart());

		Shipment ship = new Shipment();
		ship.setProvider(shipping);
		ship.setJenis(jenis);
		ship.setBerat(cart.totalBerat());

		po.setPengiriman(ship);
		return po;
	}

}
