/**
 * 
 */
package id.training.telkomsigma.marketplace.store.dto;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import lombok.Data;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Data
public class PurchaseOrder {
	private Date waktuTransaksi = new Date();
    private List<CartItem> daftarBelanja = new ArrayList();
    private Shipment pengiriman = new Shipment();
}
