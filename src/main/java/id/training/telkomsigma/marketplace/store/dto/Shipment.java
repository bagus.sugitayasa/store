/**
 * 
 */
package id.training.telkomsigma.marketplace.store.dto;

import java.math.BigDecimal;

import lombok.Data;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Data
public class Shipment {
	private String provider;
    private String jenis;
    private String asal;
    private String tujuan;
    private BigDecimal berat;
    private BigDecimal biaya;
}
