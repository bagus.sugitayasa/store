/**
 * 
 */
package id.training.telkomsigma.marketplace.store.dto;

import lombok.Data;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Data
public class ProductPhotos {
	private String id;
    private Product product;
    private String url;
}
