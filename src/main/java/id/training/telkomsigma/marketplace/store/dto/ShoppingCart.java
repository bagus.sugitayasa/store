/**
 * 
 */
package id.training.telkomsigma.marketplace.store.dto;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import lombok.Data;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Data
public class ShoppingCart {
	private List<CartItem> isiCart = new ArrayList();

    public BigDecimal totalBerat(){
        BigDecimal total = BigDecimal.ZERO;
        for (CartItem ci : isiCart) {
            total = total.add(ci.getProduct().getWeight().multiply(BigDecimal.valueOf(ci.getJumlah())));
        }
        return total;
    }

}
