/**
 * 
 */
package id.training.telkomsigma.marketplace.store.dto;

import lombok.Data;

/**
 * @author <a href="mailto:bagus.sugitayasa@sigma.co.id">GusdeGita</a>
 * @version Id: 
 */
@Data
public class CartItem {
	private Product product;
	private Integer jumlah;
}
